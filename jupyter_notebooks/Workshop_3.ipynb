{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 3: Sampling\n",
    "by Leslie Kerby with some material adapted from Berkeley Data Science"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, set up the imports by running the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. If Statements and Sampling\n",
    "\n",
    "**Conditional Statements (IF statement)**<br/>\n",
    "In Python, the format for `if` statements is:\n",
    "\n",
    "```\n",
    "if <if expression>:\n",
    "    <if body>\n",
    "elif <elif expression 0>:\n",
    "    <elif body 0>\n",
    "elif <elif expression 1>:\n",
    "    <elif body 1>\n",
    "...\n",
    "else:\n",
    "    <else body>\n",
    "```\n",
    "\n",
    "Only one of the bodies will ever be executed. Each `if` and `elif` expression is evaluated and considered in order, starting at the top. As soon as a true value is found, the corresponding body is executed, and the rest of the expression is skipped. If none of the `if` or `elif` expressions are true, then the `else body` is executed. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "** Dungeons & Dragons **<br/>\n",
    "In the game Dungeons & Dragons, each player plays the role of a fantasy character.\n",
    "\n",
    "A player performs actions by rolling a 20-sided die, adding a \"modifier\" number to the roll, and comparing the total to a threshold for success.  The modifier depends on her character's competence in performing the action.\n",
    "\n",
    "For example, suppose Rachel's character, a Ranger named Zelda, is trying to sneak across a monster-infested camp.  She rolls a 20-sided die, adds a modifier of 10 to the result (because her character is good at stealth), and succeeds if the total is greater than 14.\n",
    "\n",
    "** Question 1.1 ** <br/>Write code that simulates this procedure.  Compute three values: the result of Rachel's roll (`roll_result`), the result of her roll plus Zelda's modifier (`modified_result`), and a boolean value indicating whether the action succeeded (`action_succeeded`).  \n",
    "\n",
    "To randomly choose (with replacement) from an array use the `numpy` method `np.random.choice(array_name, num_samples)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "for_assignment_type": "student"
   },
   "outputs": [],
   "source": [
    "...\n",
    "\n",
    "# The next line just prints out your results in a nice way\n",
    "# once you're done.  You can delete it if you want.\n",
    "print(\"On a modified roll of {:d}, Rachel's action {}.\".format(modified_result, \"succeeded\" if action_succeeded else \"failed\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "** Question 1.2 ** <br/>Run your cell 7 times to manually estimate the chance that Alice succeeds at this action.  (Don't use math or an extended simulation.). Your answer should be a fraction. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rough_success_chance = ...\n",
    "rough_success_chance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose we don't know that Zelda has a modifier of 10 for this action.  Instead, we observe the modified roll (that is, the die roll plus the modifier of 10) from each of *n* attempts to sneak across the camp.  We would like to estimate her modifier from these *n* numbers.\n",
    "\n",
    "** Question 1.3 ** <br/>Write a Python function called `simulate_observations`.  It should take one argument, the number *n* of attempts to simulate, and it should return an array of *n* modified rolls. Then call your function once to compute an array of 100 simulated modified rolls.  Name that array `observations`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "** Question 1.4 ** <br/>Draw a histogram to display the *probability distribution* of the modified rolls we might see.\n",
    "\n",
    "Note that `hist` is a valid method of DataFrames, so can be called as `df.hist` (and it uses matplotlib under the hood). However, `numpy` does not have this `hist` method, and so it must be directly called from `matplotlib pyplot`, which is commonly renamed as `plt`. In using `plt.hist`, the `numpy` array is passed as the first argument (as opposed to a DataFrame where the `hist` method is called on the object itself).\n",
    "\n",
    "Also note that the `hist` keyword `density=True` will plot a probability distribution instead of the frequency of each bin (ie it divides the tally of each bin by the total number of observations)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.style.use('fivethirtyeight')\n",
    "\n",
    "observations = simulate_observations(100000)\n",
    "plt.hist(observations, bins=20, density=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your histogram should have values 11 to 30 each with a probability of 5%.\n",
    "\n",
    "Now let's imagine we don't know the modifier and try to estimate it from `observations`.\n",
    "\n",
    "One straightforward way to do that is to find the *smallest* total roll, since the smallest roll on a 20-sided die is 1.\n",
    "\n",
    "** Question 1.5 ** <br/>Using that method, estimate `modifier` from `observations`.  Name your estimate `min_estimate`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way to estimate the modifier involves the mean of `observations`.\n",
    "\n",
    "** Question 1.6 ** <br/>Figure out a good estimate based on that quantity.  \n",
    "\n",
    "**Then**, write a function named `mean_based_estimator` that computes your estimate.  It should take an array of modified rolls (like the array `observations`) as its argument and return an estimate of `modifier` based on those numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. For Loops and Monty Hall\n",
    "\n",
    "### 2.1. Iteration\n",
    "\n",
    "Using a `for` statement, we can perform a task multiple times. This is known as iteration. The format of `for` loops in Python is shown below.\n",
    "\n",
    "```\n",
    "for <element> in <collection>:\n",
    "    <for_loop body>\n",
    "```\n",
    "The `element` variable is a dummy variable and can be called whatever you want to call it; it simply iterates through the `collection`/container (ie an array, DataFrame, vector, list, etc).\n",
    "\n",
    "Here, we'll simulate drawing different suits from a deck of cards. \n",
    "\n",
    "Note that we set up an empty `draws` array before the `for` loop. We `append` to it within the `for` loop each time we draw a new card."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "suits = np.array([\"♤\", \"♡\", \"♢\", \"♧\"])\n",
    "\n",
    "draws = np.array([])\n",
    "\n",
    "repetitions = 6\n",
    "for i in np.arange(repetitions):\n",
    "    draws = np.append(draws, np.random.choice(suits))\n",
    "\n",
    "draws"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The astute observer will note that we could have randomly selected our 6 cards a much simpler way, and without the use of a `for` loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "suits = np.array([\"♤\", \"♡\", \"♢\", \"♧\"])\n",
    "draws = np.random.choice(suits, 6)\n",
    "draws"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, most of the time the simulation we are repeating is far more complex than simply selecting a suit. In these cases, a `for` loop will be used to run the simulation *n* times. We will see this soon when we simulate the Monty Hall problem.\n",
    "\n",
    "Another use of iteration is to loop through a set of values. For instance, we can print out all of the colors of the rainbow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rainbow = np.array([\"red\", \"orange\", \"yellow\", \"green\", \"blue\", \"indigo\", \"violet\"])\n",
    "\n",
    "for color in rainbow:\n",
    "    print(color)\n",
    "\n",
    "#rainbow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the indented part of the `for` loop, known as the body, is executed once for each item in `rainbow`. Note that the name `color` is arbitrary; we could easily have named it something else. The important thing is we stay consistent throughout the for loop. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for x in rainbow:\n",
    "    print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, however, we would like the variable name to be somewhat informative. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2. The Monty Hall Problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Originating from the American TV program, \"Let's Make a Deal,\" the Monty Hall problem gives a contestant a chance to win a car. There are three doors and behind one is a brand new car. The other two doors are hiding goats. \n",
    "\n",
    "The contestant chooses a door: the first, second, or third. Then the host Monty Hall opens up one of the other two doors remaining to reveal a goat. There are now two unopened doors, the one the contestant originally chose and the other un-opened door. Behind one is a goat and behind the other is a car. The contestant is given the option to stay with their originally-chosen door or to switch to the other door. Which should they do? Or does it not matter?\n",
    "\n",
    "Create a python simulation to answer this question. \n",
    "\n",
    "**Step 1.** Create a numpy array `doors` with the three possibiliites: `goat one`, `goat two`, and `car`. The contestant randomly chooses a door; simulate this by randomly choosing one of these three using `np.random.choice()` and calling it `original_choice`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 2.** Now the host reveals a goat behind one of the two un-chosen doors. If the contestant had chosen a door with a goat behind it, then there is only one other door with a goat and that one will be opened by the host. If the contestant had chosen the door with the car behind it, then both un-chosen doors contain goats and the host will randomly decide which one to open. Simulate this. Call the goat-door the host reveals `revealed_door`. <br/>\n",
    "*Hint:* Use `if` statements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 3.** Copy your code from Step 2 and modify it to assign what is left behind the third (un-chosen and un-revealed) door within the `if` statements, so as to be more computationally concise. Call this door the `other_door`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 4.** Create a variable named `choice` that will simulate whether the contestant decides to stay with their original door or switch. `choice` will be either the string \"stay\" or \"switch\". Create a variable `final_choice`, which will be equal to `original_choice` if the contestant chose to stay, and equal to `other_door` if the contestant chose to switch. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 5.** Now you have everything you need to create a function `monty_hall` which will run one simulation of the game. The function will take one parameter, `choice`, and will return the `final_choice` of the contestant. Test your new function by calling it with both 'stay' and 'switch' and see if the contestant wins a goat or a car."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 6.** Find the probability of winning a car if the contestant chooses to stay with their original choice. Do this by running `monty_hall('stay')` 10,000 times, each time appending what the contestant won to a numpy array called `winnings`. Then count how many times the contestant won the car, and divide by how many trials there were, to arrive at the final odds of winning a car when choosing to stick with the original door. Call this `winning_chance`.<br/>\n",
    "*Hint:* Use a `for` loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 7.** Now find the probability of winning a car if the contestant chooses to switch doors (do the same thing as Step 6 but for the contestant choosing the other door)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**So should you stick with your original choice or switch doors? And are you surprised at the result?**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. More Sampling (Optional)\n",
    "The data used will contain salary data and statistics for basketball players from the 2014-2015 NBA season. This data was collected from [basketball-reference](http://www.basketball-reference.com) and [spotrac](http://www.spotrac.com)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the cell below to load the player and salary data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "player_data = pd.read_csv(\"player_data.csv\")\n",
    "salary_data = pd.read_csv(\"salary_data.csv\")\n",
    "# We could add the Salary column as a new column in player_data, however we want to make sure the column is added by proper name\n",
    "# Note that these are NOT in the same player order so we have to use a more sophisticated join method\n",
    "#print(player_data.head(5))\n",
    "#print(salary_data.head(5))\n",
    "#player_data\n",
    "full_data = pd.merge(player_data, salary_data, left_on='Name',right_on='PlayerName')\n",
    "full_data.drop('PlayerName', inplace=True, axis = 1) # 1 specifies column, otherwise defaults to 0 and tries to drop a row called 'PlayerName'\n",
    "full_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rather than getting data on every player, imagine that we had gotten data on only a smaller subset of the players.  For 492 players, it's not so unreasonable to expect to see all the data, but usually we aren't so lucky.  Instead, we often make *statistical inferences* about a large underlying population using a smaller sample.\n",
    "\n",
    "A statistical inference is a statement about some statistic of the underlying population, such as \"the average salary of NBA players in 2014 was $3\".  You may have heard the word \"inference\" used in other contexts.  It's important to keep in mind that statistical inferences, unlike, say, logical inferences, can be wrong.\n",
    "\n",
    "A general strategy for inference using samples is to estimate statistics of the population by computing the same statistics on a sample.  This strategy sometimes works well and sometimes doesn't.  The degree to which it gives us useful answers depends on several factors, and we'll touch lightly on a few of those today.\n",
    "\n",
    "One very important factor in the utility of samples is how they were gathered.  We have prepared some example sample datasets to simulate inference from different kinds of samples for the NBA player dataset.  Later we'll ask you to create your own samples to see how they behave."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To save typing and increase the clarity of your code, we will package the loading and analysis code into two functions. This will be useful in the rest of the lab as we will repeatedly need to create histograms and collect summary statistics from that data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4.1**. <br/>Complete the `histograms` function, which takes a DataFrame with columns `Age` and `Salary` and draws a histogram for each one. Use the min and max functions to pick the bin boundaries so that all data appears for any DataFrame passed to your function. Use the same bin widths as before (1 year for `Age` and $1,000,000 for `Salary`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def histograms(df):\n",
    "    ages = df['Age']\n",
    "    salaries = df['Salary']\n",
    "    age_bins = max(ages) - min(ages) + 1\n",
    "    salary_bins = int( (max(salaries) - min(salaries))/1000000 ) \n",
    "    df.hist('Age',bins=age_bins)\n",
    "    df.hist('Salary', bins=salary_bins)\n",
    "    \n",
    "histograms(full_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4.2**. <br/>Create a function called `compute_statistics` that takes a DataFrame containing ages and salaries and:\n",
    "- Draws a histogram of ages\n",
    "- Draws a histogram of salaries\n",
    "- Returns a two-element array containing the average age and average salary\n",
    "\n",
    "You can call your `histograms` function to draw the histograms!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_statistics(df):\n",
    "    histograms(df)\n",
    "    age = df['Age'].mean()\n",
    "    salary = df['Salary'].mean()\n",
    "    return np.array([age, salary])\n",
    "\n",
    "full_stats = compute_statistics(full_data)\n",
    "full_stats\n",
    "#print('Hi')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convenience sampling\n",
    "One sampling methodology, which is **generally a bad idea**, is to choose players who are somehow convenient to sample.  For example, you might choose players from one team that's near your house, since it's easier to survey them.  This is called, somewhat pejoratively, *convenience sampling*.\n",
    "\n",
    "Suppose you survey only *relatively new* players with ages less than 22.  (The more experienced players didn't bother to answer your surveys about their salaries.)\n",
    "\n",
    "**Question 4.3**  <br/>Assign `convenience_sample_data` to a subset of `full_data` that contains only the rows for players under the age of 22."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "convenience_sample = full_data[ full_data['Age'] < 22 ]\n",
    "convenience_sample"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4.4** <br/>Assign `convenience_stats` to a list of the average age and average salary of your convenience sample, using the `compute_statistics` function.  Since they're computed on a sample, these are called *sample averages*. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "convenience_stats = compute_statistics(convenience_sample)\n",
    "convenience_stats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we'll compare the convenience sample salaries with the full data salaries in a single histogram. The following cell should not require any changes; just run it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compare_salaries(first, second, first_title, second_title):\n",
    "    \"\"\"Compare the salaries in two DataFrames.\"\"\"\n",
    "    plt.clf()\n",
    "    salary_bins_first =int( (max(first['Salary']) - min(first['Salary']))/1000000 )\n",
    "    salary_bins_second =  int( (max(second['Salary']) - min(second['Salary']))/1000000 ) \n",
    "    plt.hist(first['Salary'], bins=salary_bins_first, alpha=0.5, label=first_title)\n",
    "    plt.hist(second['Salary'], bins=salary_bins_second, alpha=0.5, label=second_title)\n",
    "    plt.legend(loc='upper right')\n",
    "    plt.show()\n",
    "\n",
    "compare_salaries(full_data, convenience_sample, 'All Players', 'Convenience Sample')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simple random sampling\n",
    "A more principled approach is to sample uniformly at random from the players.  If we ensure that each player is selected at most once, this is a *simple random sample without replacement*, sometimes abbreviated to \"simple random sample\" or \"SRSWOR\".  Imagine writing down each player's name on a card, putting the cards in an urn, and shuffling the urn.  Then, pull out cards one by one and set them aside, stopping when the specified *sample size* is reached.\n",
    "\n",
    "Pandas DataFrames provide the method `sample()` for producing random samples.  Note that its default is to sample WITHOUT replacement (ie you cannot draw the same row twice). \n",
    "\n",
    "Create two samples: one a sample of size 44 (the same as the convenience sample) and a larger sample of size 100.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "small_data = full_data.sample(44) # default for DataFrame sample IS without replacement (np.random.choice default was replace=True)\n",
    "small_data\n",
    "large_data = full_data.sample(100)\n",
    "large_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4.5** <br/>Run the same analyses on the small and large samples that you previously ran on the full dataset and on the convenience sample.  Compare the accuracy of the estimates of the population statistics that we get from the convenience sample, the small simple random sample, and the large simple random sample. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Full data stats:                 ', compute_statistics(full_data))\n",
    "print('Small simple random sample stats:', compute_statistics(small_data))\n",
    "print('Large simple random sample stats:', compute_statistics(large_data))\n",
    "compare_salaries(full_data, small_data, 'All Players', 'Small Sample')\n",
    "compare_salaries(full_data, large_data, 'All Players', 'Large Sample')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4.6** <br/>\n",
    "Create new samples of size 44 several times and think about the following questions: \n",
    "- How much does the average age change across samples? \n",
    "- What about average salary?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the results are similar, but not the same, with each random sample. The average age tends to stay around the same value as there is a limited range of ages for NBA players, but the salary changes by a sizeable factor due to larger variability in salary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4.7** <br/>As in the previous question, analyze several simple random samples of size 100 from `full_data`.  \n",
    "- Do the histogram statistics seem to change more or less across samples of 100 than across samples of size 44?  \n",
    "- Are the sample averages and histograms closer to their true values for age or for salary?  What did you expect to see?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The average and histogram statistics seem to change less across samples of this size. They are closer to their true values, which is what we'd expect to see because we are sampling a larger subset of the population. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
