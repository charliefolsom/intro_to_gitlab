
# GitLab





## Rules of GitLab


1. Use feature branches, no direct commits on master (When using Git you should create a branch for whatever you’re working on, so that you end up doing a code review before you merge.)
2. Everyone starts from master, and targets master: This means you don’t have any long branches. You check out master, build your feature, create your merge request, and target master again. You should do your complete review before you merge, and not have any intermediate stages.
3. Commit messages reflect intent: You should not only say what you did, but also why you did it. It’s even more useful if you explain why you did this over any other options.


## Application for GitLab


1. Anything in the master branch is deployable
2. create descriptive branches off of master: To work on something new, create a descriptively named branch off of master (ie: new-oauth2-scopes)
3. push to named branches constantly: Commit to that branch locally and regularly push your work to the same named branch on the server (**git fetch** will allow you to see who is working on what)
4. open a pull request at any time: When you need feedback or help, or you think the branch is ready for merging, open a pull request (If the branch has been open for too long and you feel it’s getting out of sync with the master branch, you can merge master into your topic branch and keep going)
5. merge only after pull request review: After someone else has reviewed and signed off on the feature, you can merge it into master


### Using GitLab


1. Cloning repository::

    git clone URL
    git clone https://hpcgitlab.inl.gov/experiment_analysis/documentation

2. Check your repository::

    git status

3. Creating a branch if updating::

    git checkout -b adding_readme_file


4. Adding files to repository::

    git add README.md  #Add individual file

    git add . #add all files in directory

    git add -i # intereactive prompt

5. Committing changes::

    git commit -m "Added readme file"

6. Send it to gitlab server::

    git push -u origen adding_readme_file

7. Create merge request (https://learning.oreilly.com/library/view/gitlab-cookbook/9781783986842/ch04s02.html)

    * Go to your GitLab instance and log in
    * Go to your project.
    * Click on Merge Requests.
    * Click on New Merge Requestion
    * Click Submit merge request

8. Accepting a merge request (https://learning.oreilly.com/library/view/gitlab-cookbook/9781783986842/ch04s03.html)

    * Go to your GitLab instance and log in
    * Go to your project.
    * Click on Merge Requests.
    * Can accept the request automatically, remove source-branch, and accept merge request


## Detailed information about GitLab Commands



### How to generate branches


Only have feature branches and a master branch:

* Tell Git to create a new branch and decide what name it should have::

    git branch [descritive_branch_off_of_master]

* At this point, we're not working on the new branch yet. Let's switch to the new branch::

    git checkout [descritive_branch_off_of_master]

* Both commands at once::

    git checkout -b [descritive_branch_off_of_master]


### What to do after having branch


What to do after branch::

    # Update local
    git pull origen master

    # Makes changes

    # Stage changes
    git add numbers.txt

    # Commit changes
    git commit -m "Add 100 to numbers.txt"

    # Push to descritive_branch
    git push -u origin [descritive_branch_off_of_master]

### Ignoring things in git


See what git is saying::

    git status

    vi .gitignore

Add data to the .gitignore::

    git add .gitignore
    git commit -m "Ignore data files and the results folder."
    git status

    # To see what is ignored
    git status --ignored



## Setting up GitLab


https://learning.oreilly.com/library/view/gitlab-cookbook/9781783986842/ch02s02.html

### Generate SSH key

In the following steps, we will create an SSH key for your Unix system:

First, we are going to check whether you have any SSH keys present. Open your terminal and enter the following command::

    ls ~/.ssh

If you have a file named id_rsa.pub or id_dsa.pub, you can skip this recipe.

To generate a new SSH key, you need to open your terminal and enter the following command::

    ssh-keygen -t rsa -C "Comment for key"

The comment can be anything you want; it makes it easier for you to see which key it is when you open the key file.


For Windows go here::

    https://learning.oreilly.com/library/view/gitlab-cookbook/9781783986842/ch02s03.html

To set up your SSH key, perform the following steps:

Open GitLab and go to your account settings.

Click on SSH.

Click on Add SSH Key::

    cat ~/.ssh/id_rsa.pub

You should copy the entire content of the output in step 4, as shown in the following screenshot:


We now paste the content of the SSH key into the form in your GitLab instance. You can name the SSH key anything you want. It is recommended that you name it after the computer it came from. This way, it will be easier to know which key belongs to which machine. If you leave the name field empty, GitLab will generate a name for you.

Now, click on Add Key.

### Creating projects


* Gitlab https://hpcgitlab.inl.gov/dashboard/projects
* Create project
* Create an SSH key
* Git config appropriatly::

    git config --global user.name "Biff Meerkat"
    git config --global user.email "biff.meerkat@example.com"

* Clone repository::

    git clone git@gitlab.com:your_username/monoROT13.git

    # Or if existing folder
    cd existing_folder
    git init
    git remote add origin git@gitlab.com:your_username/monoROT13.git

    # Existing repository
    cd existing_repo
    git remote add origin git@gitlab.com:your_username/monoROT13.git
    git push -u origin --all
    git push -u origin --tags

* Add project members


### Other commands to think about


Other commands::

    # Fetch changes from the remote but not update tracking branches
    git fetch [remote]

    # See which branch you are on
    git branch -a

    # Remove a allready commited branch that has been merged
    git branch -d working_on_git_document

    # What is changed but not staged
    git diff

    # Show the commit history ofr the currently active branch
    git log

    # Merge a remote branch into your curent branch to bring it up to date
    git merge [alias]/[branch]

    # Looking at all the branches remotely
    git branch -r

Advanced Features

Git lab runners installation on a mac can be found https://docs.gitlab.com/runner/install/osx.html

Commands are::

     brew install gitlab-runner
     brew services start gitlab-runner
     gitlab-runner register

     # Stop service
     gitlab-runner stop

    # Permission to exicute
    chmod +x /usr/local/bin/gitlab-runner


## References


### General references



GitLab Quick Start Guide https://learning.oreilly.com/library/view/gitlab-quick-start/9781789534344/

GitLab CookBook (Chapter 2, Chapter 4)  https://learning.oreilly.com/library/view/gitlab-cookbook/9781783986842/

Software Carpentry (Version control with Git) http://swcarpentry.github.io/git-novice/

Gitlab video tutorial  https://learning.oreilly.com/videos/learning-gitlab/9781789809169/9781789809169-video1_1

.. note::  You can get access to learning.orielly.com by email the INL library for an account (no charge number required)

### GitLab Workflow


https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/

https://about.gitlab.com/solutions/gitlab-flow/

https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/

http://scottchacon.com/2011/08/31/github-flow.html



### Get Cheat Sheet


https://education.github.com/git-cheat-sheet-education.pdf

https://about.gitlab.com/images/press/git-cheat-sheet.pdf

https://git-scm.com/docs





